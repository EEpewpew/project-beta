from django.db import models

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    

class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.PositiveIntegerField()

    def __str__(self):
        return self.first_name


class Appointment(models.Model):

    date_time = models.DateTimeField()
    is_vip = models.BooleanField(default=False)
    reason = models.TextField()
    status = models.CharField(max_length=100, default='pending')
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        self.status = 'canceled'
        self.save()

    def finish(self):
        self.status = 'finished'
        self.save()    

    def __str__(self):
        return f"Appointment {self.pk}"

