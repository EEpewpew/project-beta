import json
from django.http import JsonResponse
from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
            ######  LIST TECHNICIANS
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
            ######  CREATE TECHNICIAN
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not add Technician"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_delete_technician(request, pk):
            ######  SHOW TECHNICIAN
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"},
            status=404,)
            ######  DELETE TECHNICIAN
    elif request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0}
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"},
            status=404,)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
            ###### LIST APPOINTMENTS
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            inventory_vins = AutomobileVO.objects.filter(sold=True).values_list("vin", flat=True)
            serialized_appointments = []

            for appointment in appointments:
                appointment_data = AppointmentEncoder().default(appointment)
                appointment_data["is_vip"] = appointment.vin in inventory_vins
                serialized_appointments.append(appointment_data)

            return JsonResponse(
                {"appointments": serialized_appointments},
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment not found"},
            status=404,)
    else:   ######  CREATE APPOINTMENT
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
            content["status"] = "pending"
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to create appointment"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_appointment(request, pk):
            ######  SHOW APPOINTMENT
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment not found"},
            status=404,)
            ######  DELETE APPOINTMENT
    elif request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment not found"},
            status=404,)
        

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
            ######  CANCEL APPOINTMENT
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Appointment not found"},
            status=404,
        )


@require_http_methods(["PUT"])
def api_finish_appointment(prequest, pk):
            ######  FINISH APPOINTMENT
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Appointment not found"},
            status=404,
        )
