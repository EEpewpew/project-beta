from django.shortcuts import render
from .models import AutomobileVO, Salesperson, Sale, Customer
from .encoders import (
    SalespersonListEncoder,
    SalesListEncoder,
    CustomerListEncoder,
    )
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import requests

# Create your views here.


# Create a view for salespeople list
@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create salesperson"}
            )
            response.status_code = 400
            return response


# Salesperson details
@require_http_methods(["DELETE", "GET"])
def api_show_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "They don't even work here"})
            response.status_code = 400
            return response
    else:
        salesperson = Salesperson.objects.get(id=id)
        salesperson.delete()
        return JsonResponse(
            {"Salesperson": "going going gone"}
        )


# create list customers
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


# show customer detail
@require_http_methods(["DELETE", "GET"])
def api_show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exist"})
            response.status_code = 400
            return response
    else:
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse(
            {"customer": "has left the building"}
        )


# create a view list for sale
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        print(content)
        vin = content.get("vin")
        employee_id = content.get("employee_id")
        price = content.get("price")
        phone_number = content.get("phone_number")

        if vin and employee_id and price and phone_number:
            try:
                automobile = AutomobileVO.objects.get(vin=vin)
                salesperson = Salesperson.objects.get(employee_id=employee_id)
                customer = Customer.objects.get(phone_number=phone_number)

                sale = Sale.objects.create(
                    automobile=automobile,
                    salesperson=salesperson,
                    customer=customer,
                    price=price
                )
                # Making a PUT request
                requests.put(
                    f'http://project-beta-inventory-api-1:8000/api/automobiles/{vin}/',
                    json={'sold': True}
                    )

                return JsonResponse(
                    {"sale": sale.id}
                )
            except (
                AutomobileVO.DoesNotExist,
                Salesperson.DoesNotExist,
                Customer.DoesNotExist
            ):
                response = JsonResponse(
                    {"message": "Invalid VIN, employee ID, or phone number"}
                )
                response.status_code = 400
                return response
        else:
            response = JsonResponse(
                {"message": "Missing required fields"}
            )
            response.status_code = 400
            return response

# delete sale
@require_http_methods(["DELETE"])
def api_sale_details(request, id):
    if request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted:": count > 0}, status=200 if count > 0 else 404
            )
