# Generated by Django 4.0.3 on 2023-06-07 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0005_rename_import_vin_automobilevo_vin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='price',
            field=models.IntegerField(),
        ),
    ]
