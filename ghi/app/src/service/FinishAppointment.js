async function finishAppointment(appointmentId) {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/finish`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ status: 'finished' }),
      });
  
      if (response.ok) {
        console.log('Appointment marked as finished successfully');
      } else {
        console.error('Failed to mark the appointment as finished');
      }
    } catch (error) {
      console.error('An error occurred while marking the appointment as finished:', error);
    }
  }
  
export default finishAppointment;