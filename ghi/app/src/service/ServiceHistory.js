import React, { useEffect, useState } from 'react';

function HistoryAppointments() {
  const [appointments, setAppointments] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');

  async function fetchAppointments() {
    try {
      const response = await fetch('http://localhost:8080/api/appointments');

      if (response.ok) {
        const data = await response.json();
        const updatedAppointments = data.appointments.map(appointment => {
          const isVIP = appointment.is_vip === true;
          return {
            ...appointment,
            isVIP: isVIP
          };
        });
        setAppointments(updatedAppointments);
      } else {
        console.error('Failed to retrieve appointment details');
      }
    } catch (error) {
      console.error('An error occurred while retrieving appointment details:', error);
    }
  }

  useEffect(() => {
    fetchAppointments();
  }, []);

  const handleSearch = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/appointments');

      if (response.ok) {
        const data = await response.json();
        const updatedAppointments = data.appointments.map(appointment => {
          const isVIP = appointment.is_vip === true;
          return {
            ...appointment,
            isVIP: isVIP
          };
        });
        const filtered = updatedAppointments.filter(appointment =>
          appointment.vin.toLowerCase().includes(searchQuery.toLowerCase())
        );
        setAppointments(filtered);
      } else {
        console.error('Failed to retrieve appointment details');
      }
    } catch (error) {
      console.error('An error occurred while retrieving appointment details:', error);
    }
  };

      return (
        <div>
          <h2>Service History</h2>
          <form onSubmit={handleSearch}>
            <input
              type="text"
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
              placeholder="Enter VIN"
            />
            <button type="submit">Search</button>
          </form>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>VIP</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              {appointments
                .filter((appointment) =>
                  appointment.vin.toLowerCase().includes(searchQuery.toLowerCase())
                )
                .map((appointment) => (
                  <tr key={appointment.id}>
                    <td>{appointment?.vin}</td>
                    <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                    <td>{appointment?.customer}</td>
                    <td>
                      {new Date(appointment?.date_time).toLocaleDateString()}
                    </td>
                    <td>
                      {new Date(appointment?.date_time).toLocaleTimeString([], {
                        hour: '2-digit',
                        minute: '2-digit',
                      })}
                    </td>
                    <td>
                      {`${appointment?.technician?.first_name} ${appointment?.technician?.last_name}`}
                    </td>
                    <td>{appointment?.reason}</td>
                    <td>{appointment?.status}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      );
  }
  
  export default HistoryAppointments;