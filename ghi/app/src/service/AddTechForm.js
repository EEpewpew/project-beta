import React, { useState } from 'react';
import './TechForm.css';

function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [error, setError] = useState('');

    const handleSubmit = async (event) => {
      event.preventDefault();

      try {
        const data = {
          first_name: firstName,
          last_name: lastName,
          employee_id: employeeId,
        };

        const technicianUrl = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };

        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
          const newTechnician = await response.json();
          console.log(newTechnician);

          setFirstName('');
          setLastName('');
          setEmployeeId('');
          setError('');
        } else {
          setError("Failed to create technician");
        }
      } catch (error) {
        setError("An error occurred");
      }
    };

    const handleFirstNameChange = (event) => {
      setFirstName(event.target.value);
    };

    const handleLastNameChange = (event) => {
      setLastName(event.target.value);
    };

    const handleEmployeeIdChange = (event) => {
      setEmployeeId(event.target.value);
    };

    return (
        <div className="form-container">
          <form onSubmit={handleSubmit} id="create-technician-form" className="form">
          <h2>Add a Technician</h2>
            <label className="form-label">
              First Name:
              <input
                type="text"
                value={firstName}
                onChange={handleFirstNameChange}
                className="form-input"
              />
            </label>
            <br />
            <label className="form-label">
              Last Name:
              <input
                type="text"
                value={lastName}
                onChange={handleLastNameChange}
                className="form-input"
              />
            </label>
            <br />
            <label className="form-label">
              Employee ID:
              <input
                type="text"
                value={employeeId}
                onChange={handleEmployeeIdChange}
                className="form-input"
              />
            </label>
            <br />
            {error && <p className="error">Error: {error}</p>}
            <button type="submit" className="btn-submit">Create</button>
          </form>
        </div>
    );
}

export default TechnicianForm;
