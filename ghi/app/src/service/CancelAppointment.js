async function cancelAppointment(appointmentId) {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/cancel`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ status: 'canceled' }),
      });
  
      if (response.ok) {
        console.log('Appointment canceled successfully');
      } else {
        console.error('Failed to cancel the appointment');
      }
    } catch (error) {
      console.error('An error occurred while canceling the appointment:', error);
    }
  }
  
export default cancelAppointment;