import React, { useEffect, useState } from 'react';
import deleteAppointment from './DeleteAppointment';
import cancelAppointment from './CancelAppointment';
import finishAppointment from './FinishAppointment';


function ListAppointments() {
  const [appointments, setAppointments] = useState([]);

  async function fetchAppointments() {
    try {
      const response = await fetch('http://localhost:8080/api/appointments');

      if (response.ok) {
        const data = await response.json();
        const updatedAppointments = data.appointments.map(appointment => {
            const isVIP = appointment.is_vip === true;
            return {
              ...appointment,
              isVIP: isVIP
            };
          });
        setAppointments(updatedAppointments);
      } else {
        console.error('Failed to retrieve appointment details');
      }
    } catch (error) {
      console.error('An error occurred while retrieving appointment details:', error);
    }
  }

  useEffect(() => {
    fetchAppointments();
  }, []);

  async function handleCancelAppointment(appointmentId) {
    await cancelAppointment(appointmentId);
    fetchAppointments();
  }

  async function handleFinishAppointment(appointmentId) {
    await finishAppointment(appointmentId);
    fetchAppointments();
  }

  async function handleDeleteAppointment(appointmentId) {
    await deleteAppointment(appointmentId);
    fetchAppointments();
  }

  return (
    <div>
    <h2>Service Appointments</h2>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>VIP</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>Actions</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {appointments.map((appointment) => {
          if (appointment.status !== 'pending') {
            return null; 
          }

          const appointmentDate = new Date(appointment.date_time);
          const formattedDate = appointmentDate.toLocaleDateString();
          const formattedTime = appointmentDate.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
  
          return (
            <tr key={appointment.id}>
              <td>{appointment?.vin}</td>
              <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
              <td>{appointment?.customer}</td>
              <td>{formattedDate}</td>
              <td>{formattedTime}</td>
              <td>{`${appointment?.technician?.first_name} ${appointment?.technician?.last_name}`}</td>
              <td>{appointment?.reason}</td>
              <td>
                <button
                    onClick={() => handleCancelAppointment(appointment.id)}
                    style={{
                    backgroundColor: 'red',
                    color: 'white',
                    border: 'none',
                    padding: '8px 16px',
                    borderRadius: '4px',
                    cursor: 'pointer'
                    }}
                >
                    Cancel
                </button>
                <button
                    onClick={() => handleFinishAppointment(appointment.id)}
                    style={{
                    backgroundColor: 'green',
                    color: 'white',
                    border: 'none',
                    padding: '8px 16px',
                    borderRadius: '4px',
                    cursor: 'pointer'
                    }}
                >
                    Finish
                </button>
              </td>
              <td>
                <button 
                    onClick={() => handleDeleteAppointment(appointment.id)}
                    style={{
                        backgroundColor: 'black',
                        color: 'white',
                        border: 'none',
                        padding: '8px 16px',
                        borderRadius: '4px',
                        cursor: 'pointer'
                        }}
                >
                    Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </div>
  );
}

export default ListAppointments;