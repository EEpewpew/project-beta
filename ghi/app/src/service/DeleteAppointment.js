async function deleteAppointment(appointmentId) {
    try {
      const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}`, {
        method: 'DELETE',
      });
  
      if (response.ok) {
        console.log('Appointment deleted successfully');
      } else {
        console.error('Failed to delete the appointment');
      }
    } catch (error) {
      console.error('An error occurred while deleting the appointment:', error);
    }
  }
  
  export default deleteAppointment;