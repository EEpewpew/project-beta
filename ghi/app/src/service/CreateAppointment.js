window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-appointment-form');
    formTag.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));
  
      const appointmentUrl = 'http://localhost:8080/api/appointments/';
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(appointmentUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newAppointment = await response.json();
        console.log(newAppointment);
      }
    });
  });
  
  
  
  
  
  