async function deleteTechnician(technicianId) {
    try {
      const response = await fetch(`http://localhost:8080/api/technicians/${technicianId}`, {
        method: 'DELETE',
      });
  
      if (response.ok) {
        console.log('Technician deleted successfully');
      } else {
        console.error('Failed to delete the technician');
      }
    } catch (error) {
      console.error('An error occurred while deleting the technician:', error);
    }
}
  
export default deleteTechnician;