import React, {useEffect, useState} from 'react';
import deleteTechnician from './DeleteTech';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    async function fetchTechnicians() {
      const response = await fetch("http://localhost:8080/api/technicians/");

      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      }
    }

    useEffect(() => {
      fetchTechnicians();
    }, []);

    async function handleDeleteTechnician(technicianId) {
      await deleteTechnician(technicianId);
      fetchTechnicians();
    }

    return (
      <div>
      <h2>Technicians</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{technician.first_name}</td>
                <td>{technician.last_name}</td>
                <td>{technician.id}</td>
                <td>
                    <button
                        onClick={() => handleDeleteTechnician(technician.id)}
                        style={{
                        backgroundColor: 'black',
                        color: 'white',
                        border: 'none',
                        padding: '8px 16px',
                        borderRadius: '4px',
                        cursor: 'pointer'
                        }}
                    >
                        Delete
                    </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
}

export default TechnicianList;
