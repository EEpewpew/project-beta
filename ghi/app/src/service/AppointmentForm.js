import React, { useState } from 'react';
import './TechForm.css';

function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [status, setStatus] = useState('');
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [technicianId, setTechnicianId] = useState('');
    const [error, setError] = useState('');
  
    const handleSubmit = async (event) => {
      event.preventDefault();
  
      if (!vin || !customer || !dateTime || !technicianId || !reason) {
        setError('Please fill in all fields');
        return;
      }
  
      const appointmentData = {
        vin,
        customer,
        date_time: dateTime,
        technician: technicianId,
        reason,
      };
  
      try {
        const response = await fetch('http://localhost:8080/api/appointments/', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(appointmentData),
        });
  
        if (response.ok) {
          const newAppointment = await response.json();
          console.log('New appointment created:', newAppointment);
  
          setVin('');
          setStatus('');
          setCustomer('');
          setDateTime('');
          setReason('');
          setTechnicianId('');
          setError('');
        } else {
          setError('Failed to create appointment');
        }
      } catch (error) {
        setError('An error occurred while creating the appointment');
        console.error(error);
      }
    };
  
    return (
        <div className="form-container">
          <div className="form">
            <h2>Create a Service Appointment</h2>
            {error && <p className="error">{error}</p>}
            <form onSubmit={handleSubmit}>
              <label className="form-label">
                VIN:
                <input
                  className="form-input"
                  type="text"
                  value={vin}
                  onChange={(e) => setVin(e.target.value)}
                />
              </label>
              <br />
              <label className="form-label">
                Status:
                <input
                  className="form-input"
                  type="text"
                  value={status}
                  onChange={(e) => setStatus(e.target.value)}
                />
              </label>
              <br />
              <label className="form-label">
                Customer:
                <input
                  className="form-input"
                  type="text"
                  value={customer}
                  onChange={(e) => setCustomer(e.target.value)}
                />
              </label>
              <br />
              <label className="form-label">
                Date and Time:
                <input
                  className="form-input"
                  type="datetime-local"
                  value={dateTime}
                  onChange={(e) => setDateTime(e.target.value)}
                />
              </label>
              <br />
              <label className="form-label">
                Reason:
                <input
                  className="form-input"
                  type="text"
                  value={reason}
                  onChange={(e) => setReason(e.target.value)}
                />
              </label>
              <br />
              <label className="form-label">
                Technician ID:
                <input
                  className="form-input"
                  type="text"
                  value={technicianId}
                  onChange={(e) => setTechnicianId(e.target.value)}
                />
              </label>
              <br />
              <button className="btn-submit" type="submit">Create</button>
            </form>
          </div>
        </div>
      );
  }
  
  export default AppointmentForm;