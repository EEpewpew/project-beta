import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './service/AddTechForm';
import TechnicianList from './service/ListTechnicians';
import Salespeople from './salespeople/ListSalespeople';
import SalespersonForm from './salespeople/CreateSalesperson';
import AppointmentForm from './service/AppointmentForm';
import ListAppointments from './service/ListAppointments';
import HistoryAppointments from './service/ServiceHistory';
import ManufacturerList from './inventory/ListManufacturers';
import ManufacturerForm from './inventory/AddManufacturerForm';
import ModelList from './inventory/ListModels';
import ModelForm from './inventory/ModelForm';
import CustomersList from './customer/ListCustomers';
import CustomerForm from './customer/CreateCustomer';
import AutoList from './inventory/ListAutomobiles';
import AutomobileForm from './inventory/AutomobileForm';
import SalesList from './sales/ListSales';
import SalesForm from './sales/SalesForm';
import SalespeopleHistory from './salespeople/SalespeopleHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/">
            <Route path="" element={<ManufacturerList />} />
            <Route path="create/" element={<ManufacturerForm />} />
          </Route>
          <Route path="models/">
            <Route path="" element={<ModelList />} />
            <Route path="create/" element={<ModelForm />} />
          </Route>
          <Route path="automobiles/">
            <Route path="" element={<AutoList />} />
            <Route path="create/" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians/">
            <Route path="" element={<TechnicianList />} />
            <Route path="create/" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route path="" element={<ListAppointments />} />
            <Route path="create/" element={<AppointmentForm />} />
            <Route path="history/" element={<HistoryAppointments />} />
          </Route>
          <Route path="salespeople/">
            <Route path="" element={<Salespeople/>} />
            <Route path="create/" element={<SalespersonForm/>} />
            <Route path="history/" element={<SalespeopleHistory/>} />
          </Route>
          <Route path="customers/">
            <Route path="" element={<CustomersList />} />
            <Route path="create/" element={<CustomerForm />} />
          </Route>
          <Route path="sales/">
            <Route path="" element={<SalesList />} />
            <Route path="create/" element={<SalesForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
