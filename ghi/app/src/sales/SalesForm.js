import React, { useState, useEffect } from "react";

export default function SalesForm() {
const [automobiles, setAutomobiles] = useState([]);
const [salespeople, setSalespeople] = useState([]);
const [customers, setCustomers] = useState([]);
const [selectedAutomobile, setSelectedAutomobile] = useState("");
const [selectedSalesperson, setSelectedSalesperson] = useState("");
const [selectedCustomer, setSelectedCustomer] = useState("");
const [price, setPrice] = useState("");

useEffect(() => {
// Fetch data for automobiles, salespeople, and customers
const fetchAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
    const data = await response.json();
    setAutomobiles(data.autos);
    }
};

const fetchSalespeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
    const data = await response.json();
    setSalespeople(data.salespeople);
    }
};

const fetchCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
    const data = await response.json();
    setCustomers(data.customers);
    }
};

fetchAutomobiles();
fetchSalespeople();
fetchCustomers();
}, []);

const handleSubmit = async (e) => {
e.preventDefault();

// Validate form input
if (!selectedAutomobile || !selectedSalesperson || !selectedCustomer || !price) {
    return;
}

try {
    const response = await fetch("http://localhost:8090/api/sales/", {
    method: "POST",
    headers: {
        "Content-Type": "application/json",
    },
    body: JSON.stringify({
        vin: selectedAutomobile,
        employee_id: selectedSalesperson,
        phone_number: selectedCustomer,
        price: price,
    }),
    });

    if (response.ok) {
    // Sale recorded successfully
    console.log("Sale recorded successfully");
    // Reset form input values
    setSelectedAutomobile("");
    setSelectedSalesperson("");
    setSelectedCustomer("");
    setPrice("");
    } else {
    // Handle the error scenario
    console.error("Failed to record the sale");
    }
} catch (error) {
    console.error("Failed to record the sale");
}

// refresh windows after submit
window.location.reload();

};


return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Record a New Sale</h1>
            <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="automobile">Automobile</label>
                <select
                id="automobile"
                className="form-select"
                value={selectedAutomobile}
                onChange={(e) => setSelectedAutomobile(e.target.value)}
                >
                <option value="">Select an automobile</option>
                {automobiles.filter((auto) => !auto.sold)
                .map((auto) => (
                <option key={auto["vin"]} value={auto["vin"]}>
                {auto["year"]} {auto["model"]["manufacturer"]["name"]} {auto["model"]["name"]} - {auto["vin"]}
                </option>
                ))}
                </select>
            </div>
            <div className="mb-3">
                <label htmlFor="salesperson">Salesperson</label>
                <select
                id="salesperson"
                className="form-select"
                value={selectedSalesperson}
                onChange={(e) => setSelectedSalesperson(e.target.value)}
                >
                <option value="">Select a salesperson</option>
                {salespeople.map((salesperson) => (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                    {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
                </select>
            </div>
            <div className="mb-3">
                <label htmlFor="customer">Customer</label>
                <select
                id="customer"
                className="form-select"
                value={selectedCustomer}
                onChange={(e) => setSelectedCustomer(e.target.value)}
                >
                <option value="">Select a customer</option>
                {customers.map((customer) => (
                    <option key={customer.phone_number} value={customer.phone_number}>
                    {customer.first_name} {customer.last_name}
                    </option>
                ))}
                </select>
            </div>
            <div className="mb-3">
                <label htmlFor="price">Price</label>
                <input
                type="number"
                id="price"
                className="form-control"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                />
            </div>
            <button type="submit" className="btn btn-primary">
                Record Sale
            </button>
            </form>
        </div>
        </div>
    </div>
);
}