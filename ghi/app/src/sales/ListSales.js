import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function SaleList() {
    const [sales, setSales] = useState([]);

    const fetchSaleData = async () => {
        const saleUrl = "http://localhost:8090/api/sales/";
        const response = await fetch(saleUrl);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    };

    useEffect(() => {
        fetchSaleData();
    }, []);


    return (
        <div>
            <table className="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Salesperson ID</th>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SaleList;
