import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function Customer() {
const [customers, setCustomers] = useState([]);

async function fetchCustomers() {
    const response = await fetch("http://localhost:8090/api/customers");
    if (response.ok) {
    const data = await response.json();
    setCustomers(data.customers);
    console.log(data.customers);
    }
}

useEffect(() => {
    fetchCustomers();
}, []);

// phone number format -- \d matches any non digit character
// \d{3} = 3 digit grouping
const formatPhoneNumber = (phoneNumber) => {
    const cleaned = ("" + phoneNumber).replace(/\D/g, "");
    const match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);
    if (match) {
    return match[1] + "-" + match[2] + "-" + match[3];
    }
    return phoneNumber;
};

return (
    <div>
    <table className="table table-hover table-striped">
        <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
        </tr>
        </thead>
        <tbody>
        {customers.map((customer) => {
            return (
            <tr key={customer.id}>
                <td>{customer.first_name}</td>
                <td>{customer.last_name}</td>
                <td>{formatPhoneNumber(customer.phone_number)}</td>
                <td>{customer.address}</td>
                <td>
                </td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>
);
}
