import React, { useEffect, useState } from "react";

export default function CustomerForm() {
    const [customers, setCustomer] = useState([]);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");

    const fetchdata = async () => {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setCustomer(data.customers);
    } else {
        throw new Error("Response not ok");
    }
    };

    useEffect(() => {
    fetchdata();
    }, []);

    const handleCreate = async (e) => {
    e.preventDefault();

    // Validate form input
    if (!firstName || !lastName || !phoneNumber || !address) {
        return;
    }

    try {
        const response = await fetch("http://localhost:8090/api/customers/", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            first_name: firstName,
            last_name: lastName,
            phone_number: phoneNumber,
            address: address,

        }),
        });

        if (response.ok) {
        // Customer created successfully
        // Fetch the updated customer list
        fetchdata();
        // Reset form input values
        setFirstName("");
        setLastName("");
        setPhoneNumber("");
        setAddress("");
        } else {
        // Handle the error scenario
        console.error("Failed to create the customer");
        }
    } catch (error) {
        console.error("Failed to create the customer");
    }
    };

    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add a Customer</h1>
            <form id="create-location-form" onSubmit={handleCreate}>
            <div className="form-floating mb-3">
                <input
                placeholder="First name"
                required
                type="text"
                name="first_name"
                className="form-control"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                />
                <label htmlFor="first_name">First name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Last name"
                required
                type="text"
                name="last_name"
                className="form-control"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                />
                <label htmlFor="last_name">Last name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Phone Number"
                required
                type="phonenumber"
                name="phone_number"
                className="form-control"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
                />
                <label htmlFor="phone_number">Phone number</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Address"
                required
                type="text"
                name="address"
                className="form-control"
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                />
                <label htmlFor="address">Address</label>
            </div>
            <button type="submit" className="btn btn-primary">
                Create
            </button>
            </form>
        </div>
        </div>
    </div>
    );
}
