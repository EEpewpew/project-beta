import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import deleteSalesperson from "./DeleteSalesperson";

export default function Salespeople() {
    const [sales, setSales] = useState([]);

    async function fetchSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople");
    if (response.ok) {
        const data = await response.json();
        setSales(data.salespeople);
        console.log(data.salespeople);
    }
    }

    useEffect(() => {
    fetchSalespeople();
    }, []);

    return (
    <div>
        <table className="table table-hover table-striped">
        <thead>
            <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            </tr>
        </thead>
        <tbody>
            {sales.map((sale) => {
            return (
                // first name and last name capitalized
                <tr key={sale.id}>
                <td>{sale.employee_id}</td>
                <td>{sale.first_name.charAt(0).toUpperCase() + sale.first_name.slice(1)}</td>
                <td>{sale.last_name.charAt(0).toUpperCase() + sale.last_name.slice(1)}</td>
                </tr>
            );
            })}
        </tbody>
        </table>
    </div>
    );
}
