import React, { useEffect, useState } from "react";

export default function SalespersonHistory() {
const [selectedSalespersonId, setSelectedSalespersonId] = useState("");
const [salesHistory, setSalesHistory] = useState([]);
const [salespeople, setSalespeople] = useState([]);

async function fetchSalesHistory(salespersonId) {
const response = await fetch(
    "http://localhost:8090/api/sales/"
);
if (response.ok) {
    const data = await response.json();
    setSalesHistory(data.sales);
}
}

async function fetchSalespeople() {
const response = await fetch("http://localhost:8090/api/salespeople/");
if (response.ok) {
    const data = await response.json();
    setSalespeople(data.salespeople);
}
}
// update
useEffect(() => {
if (selectedSalespersonId) {
    fetchSalesHistory(selectedSalespersonId);
}
fetchSalespeople();
}, [selectedSalespersonId]);

function handleSalespersonChange(event) {
const salespersonId = event.target.value;
setSelectedSalespersonId(salespersonId);
}

return (
<div>
    <h2>Salesperson History</h2>
    <label htmlFor="salesperson">Select a salesperson: </label>
    <select id="salesperson" onChange={handleSalespersonChange}>
    <option value="">-- Select Salesperson --</option>
    {salespeople.map((salesperson) => (
        <option key={salesperson.id} value={salesperson.id}>
        {salesperson.first_name} {salesperson.last_name}
        </option>
    ))}
    </select>

    <table className="table table-hover table-striped">
    <thead>
        <tr>
        <th>Salesperson</th>
        <th>Customer</th>
        <th>VIN</th>
        <th>Price</th>
        </tr>
    </thead>
    <tbody>
        {salesHistory.filter((sales) => sales.salesperson.id == selectedSalespersonId)
        .map((sale) => (
        <tr key={sale.id}>
            <td>{sale.salesperson.first_name} , {sale.salesperson.last_name}</td>
            <td>{sale.customer.first_name} , {sale.customer.last_name}</td>
            <td>{sale.automobile.vin}</td>
            <td>${sale.price}</td>
        </tr>
        ))}
    </tbody>
    </table>
</div>
);
}
