import React, { useEffect, useState } from "react";

export default function SalespersonForm() {
    const [sales, setSales] = useState([]);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [employeeId, setEmployeeId] = useState("");

    const fetchdata = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setSales(data.salespeople);
    } else {
        throw new Error("Response not ok");
    }
    };
    // loop through the salespeople
    useEffect(() => {
    fetchdata();
    }, []);

    const handleCreate = async (e) => {
    e.preventDefault();

    // Validate form input
    if (!firstName || !lastName || !employeeId) {
        return;
    }

 // Check if the employee_id already exists + pop up error
    const employeeExists = sales.some(
    (salesperson) => salesperson.employee_id === employeeId
    );
    if (employeeExists) {
    console.error("Employee ID already exists");
    window.alert("ID is taken");
    return;
    }

    try {
        const response = await fetch("http://localhost:8090/api/salespeople/", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId,
        }),
        });

        if (response.ok) {
        // Salesperson created successfully
        // Fetch the updated salesperson list
        fetchdata();
        // Reset form input values
        setFirstName("");
        setLastName("");
        setEmployeeId("");
        } else {
        // Handle the error scenario
        console.error("Failed to create the salesperson");
        }
    } catch (error) {
        console.error("Failed to create the salesperson");
    }
    };

    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add a Salesperson</h1>
            <form id="create-location-form" onSubmit={handleCreate}>
            <div className="form-floating mb-3">
                <input
                placeholder="First name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                />
                <label htmlFor="first_name">First name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Last name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                />
                <label htmlFor="last_name">Last name</label>
            </div>
            <div className="form-floating mb-3">
                <input
                placeholder="Employee ID"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
                value={employeeId}
                onChange={(e) => setEmployeeId(e.target.value)}
                />
                <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button type="submit" className="btn btn-primary">
                Create
            </button>
            </form>
        </div>
        </div>
    </div>
    );
}
