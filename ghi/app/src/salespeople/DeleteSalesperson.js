async function deleteSalesperson(salespersonId) {
  try {
    const response = await fetch(`http://localhost:8090/api/salespeople/${salespersonId}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      console.log('Salesperson deleted successfully');
    } else {
      console.error('Failed to delete the salesperson');
    }
  } catch (error) {
    console.error('An error occurred while deleting the salesperson:', error);
  }
}

export default deleteSalesperson;
