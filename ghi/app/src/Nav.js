import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownManufacturers" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownManufacturers">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/create">Add Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownModels" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownModels">
                <li><NavLink className="dropdown-item" to="/models">Models List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/create">Add Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/create">Add Auto</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><NavLink className="dropdown-item" to="/technicians">Technicians List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/create">Add Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownAppointments" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Appointments
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownAppointments">
                <li><NavLink className="dropdown-item" to="/appointments">Appointments List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Appointments History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/create">Create Appointment</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownSalespeople" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSalespeople">
                <li><NavLink className="dropdown-item" to="/salespeople">Salespeople List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/create">Add New Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/history">Salespeople History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownCustomers" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownCustomers">
                <li><NavLink className="dropdown-item" to="/customers">Customers List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/create">Add New Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownSales" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdownSales">
                <li><NavLink className="dropdown-item" to="/sales">Sales List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/create">Add New Sale</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
