import React, {useEffect, useState} from 'react';

function ModelList() {
    const [models, setModels] = useState([]);
  
    async function fetchModels() {
      const response = await fetch("http://localhost:8100/api/models/");

      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      }
    }
  
    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <table className="table table-striped">
          <thead>
          <h2>Models</h2>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
            {models.map((model) => {
              return (
                <tr key={model?.id}>
                  <td>{model?.name}</td>
                  <td>
                    <img src={model?.picture_url} width = "200"/>
                  </td>
                  <td>{model?.manufacturer?.name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      );
  }
    
  export default ModelList;