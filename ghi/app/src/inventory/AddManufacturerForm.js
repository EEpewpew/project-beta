import React, { useState } from 'react';
import './Form.css';

function ManufacturerForm() {
    const [name, setName] = useState('');
    const [error, setError] = useState('');
  
    const handleSubmit = async (event) => {
      event.preventDefault();
  
      try {
        const data = {
          name: name,
        };
  
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
  
        const fetchConfig = {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };
  
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
          const newManufacturer = await response.json();
          console.log(newManufacturer);
  
          setName('');
          setError('');
        } else {
          setError("Failed to create manufacturer");
        }
      } catch (error) {
        setError("An error occurred");
      }
    };
  
    const handleNameChange = (event) => {
      setName(event.target.value);
    };

    return (
        <div className="form-container">
          <form onSubmit={handleSubmit} id="create-manufacturer-form" className="form">
          <h2>Create a manufacturer</h2>
            <label className="form-label">
              Name
              <input
                type="text"
                value={name}
                onChange={handleNameChange}
                className="form-input"
              />
            </label>
            {error && <p className="error">Error: {error}</p>}
            <button type="submit" className="btn-submit">Create</button>
          </form>
        </div>
    );
}
  
export default ManufacturerForm;