import React, { useEffect, useState } from 'react';

function AutoList() {
  const [automobiles, setAutomobiles] = useState([]);

  async function fetchAutomobiles() {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');

      if (response.ok) {
        const data = await response.json();
        const processedAutomobiles = data.autos.map((automobile) => {
          return {
            ...automobile,
            isSold: automobile.sold === true,
          };
        });
        setAutomobiles(processedAutomobiles);
      } else {
        console.error('Failed to retrieve automobile details');
      }
    } catch (error) {
      console.error('An error occurred while retrieving automobile details:', error);
    }
  }

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  return (
    <div>
      <h2>Automobiles</h2>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map((automobile) => (
            <tr key={automobile?.id}>
              <td>{automobile?.vin}</td>
              <td>{automobile?.color}</td>
              <td>{automobile?.year}</td>
              <td>{automobile?.model?.name}</td>
              <td>{automobile?.model?.manufacturer?.name}</td>
              <td>{automobile.isSold ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutoList;