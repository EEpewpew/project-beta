window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
    }

    const formTag = document.getElementById('create-automobile-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(automobileUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newAutomobile = await response.json();
            console.log(newAutomobile);
        }
    });
});