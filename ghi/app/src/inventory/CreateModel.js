window.addEventListener('DOMContentLoaded', async () => {
    const modelsUrl = 'http://localhost:8100/api/models/';
  
    const response = await fetch(modelsUrl);
  
    if (response.ok) {
      const data = await response.json();
    }
  
    const formTag = document.getElementById('create-model-form');
    formTag.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const modelData = Object.fromEntries(formData);
      const json = JSON.stringify(modelData);
  
      const modelUrl = 'http://localhost:8100/api/models/';
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
  
      try {
        const response = await fetch(modelUrl, fetchConfig);
  
        if (response.ok) {
          formTag.reset();
          const newModel = await response.json();
          console.log(newModel);
        } else {
          console.error('Failed to create model');
        }
      } catch (error) {
        console.error('An error occurred:', error);
      }
    });
  });