import React, { useState, useEffect } from 'react';
import './Form.css';

function ModelForm() {
    const [name, setName] = useState('');
    const [picture_url, setPicture] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [manufacturers, setManufacturers] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {
        fetchManufacturers();
      }, []);

      const fetchManufacturers = async () => {
        try {
          const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
          const response = await fetch(manufacturersUrl);
          if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
          } else {
            setError('Failed to fetch manufacturers');
          }
        } catch (error) {
          setError('An error occurred');
        }
      };

    const handleSubmit = async (event) => {
      event.preventDefault();

      try {
        const data = {
        name: name,
        picture_url: picture_url,
        manufacturer_id: manufacturer,
        };

        const modelsUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
          method: "POST",
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        };

        const response = await fetch(modelsUrl, fetchConfig);
        if (response.ok) {
          const newModel = await response.json();
          console.log(newModel);

          setName('');
          setPicture('');
          setManufacturer('');
          setError('');
        } else {
          setError('Failed to create model');
        }
      } catch (error) {
        setError('An error occurred');
      }
    };

    const handleNameChange = (event) => {
      setName(event.target.value);
    };

    const handlePictureChange = (event) => {
      setPicture(event.target.value);
    };

    const handleManufacturerChange = (event) => {
      setManufacturer(event.target.value);
    };

    return (
        <div className="form-container">
          <form onSubmit={handleSubmit} id="create-model-form" className="form">
            <h2>Add a Model</h2>
            <label className="form-label">
              Model name
              <input
                type="text"
                value={name}
                onChange={handleNameChange}
                className="form-input"
              />
            </label>
            <label className="form-label">
              Picture URL
              <input
                type="text"
                value={picture_url}
                onChange={handlePictureChange}
                className="form-input"
              />
            </label>
            <label className="form-label">
              Manufacturer
              <select
                value={manufacturer}
                onChange={handleManufacturerChange}
                className="form-input"
              >
                <option value="">Select</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
            </label>
            {error && <p className="error">Error: {error}</p>}
            <button type="submit" className="btn-submit">
              Create
            </button>
          </form>
        </div>
      );
    }

export default ModelForm;
