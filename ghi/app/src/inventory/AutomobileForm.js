import React, { useState, useEffect } from 'react';
import './Form.css';

function AutomobileForm() {
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [vin, setVin] = useState('');
  const [model, setModel] = useState('');
  const [models, setModels] = useState([]);
  const [error, setError] = useState('');

  useEffect(() => {
    fetchModels();
  }, []);

  const fetchModels = async () => {
    try {
      const modelsUrl = 'http://localhost:8100/api/models/';
      const response = await fetch(modelsUrl);
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        setError('Failed to fetch models');
      }
    } catch (error) {
      setError('An error occurred');
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const data = {
        color: color,
        year: year,
        vin: vin,
        model_id: model,
      };

      console.log(data)

      const automobilesUrl = 'http://localhost:8100/api/automobiles/';

      const fetchConfig = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      };

      const response = await fetch(automobilesUrl, fetchConfig);
      if (response.ok) {
        const newAutomobile = await response.json();
        console.log(newAutomobile);

        setColor('');
        setYear('');
        setVin('');
        setModel('');
        setError('');
      } else {
        setError('Failed to create automobile');
      }
    } catch (error) {
      setError('An error occurred');
    }
  };

  const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  const handleYearChange = (event) => {
    setYear(event.target.value);
  };

  const handleVinChange = (event) => {
    setVin(event.target.value);
  };

  const handleModelChange = (event) => {
    setModel(event.target.value);
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit} id="create-automobile-form" className="form">
        <h2>Add an Automobile to inventory</h2>
        <label className="form-label">
          Color
          <input
            type="text"
            value={color}
            onChange={handleColorChange}
            className="form-input"
          />
        </label>
        <label className="form-label">
          Year
          <input
            type="number"
            value={year}
            onChange={handleYearChange}
            className="form-input"
          />
        </label>
        <label className="form-label">
          VIN
          <input
            type="text"
            value={vin}
            onChange={handleVinChange}
            className="form-input"
          />
        </label>
        <label className="form-label">
          Model
          <select
            value={model}
            onChange={handleModelChange}
            className="form-input"
          >
            <option value="">Select</option>
            {models.map((model) => (
              <option key={model.id} value={model.id}>
                {model.name}
              </option>
            ))}
          </select>
        </label>
        {error && <p className="error">Error: {error}</p>}
        <button type="submit" className="btn-submit">
          Create
        </button>
      </form>
    </div>
  );
}

export default AutomobileForm;
